## Rodando o projeto

Após baixar o projeto em sua maquina `git clone 'link projeto github'

1 ecesse o diretório raiz do projeto baixado, execute: composer install
2 ecesse o diretório raiz do projeto, execute: npm install
3 execute o comando: cp .env.example .env ou copy .env.example .env. ..
4 no passo 3 geramos um arquivo .env
5 voce deve criar um banco de dados no seu sistema gerenciador de banco de dados, e
adicionar o nome desse banco de dados em DB_DATABASE no arquivo .env (exemplo: DB_DATABASE=nome_do_meu_banco_criado)

AS DEMAIS INFORMAÇÊOS DO SEU BANCO DE DADOS DEVEM FICAR, TAMBEM , EM .env como segue:

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=nome_do_meu_banco_criado
DB_USERNAME=usuario_do_banco
DB_PASSWORD=senha_do_banco

Execute: php artisan key:generate.
Execute: php artisan migrate.
Execeute: php artisan serve (rodara o servidor php ).
Execute, em outro terminal, na raiz do projeto: npm run watch (rodara o projeto vuejs)
vá para http://localhost:3000/home/index.

para se registrar com um Adminstrador e cadastrar as receitas vá pra http://localhost:3000/register
após de registrar, para logar com um Adminstrador vá pra http://localhost:3000/login
