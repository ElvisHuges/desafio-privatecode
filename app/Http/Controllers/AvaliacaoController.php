<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\avaliacao;

class AvaliacaoController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
        public function store(Request $request)
        {
            avaliacao::create([
                'username'          => $request->username,
                'comment'         => $request->comment,
                'value'         => $request->value,
                'receita_id'      => $request->id_receita,
            ]);
            return response()->json(['msg' => 'Registered Successfully']);
        }
}
