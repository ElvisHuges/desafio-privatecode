<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Receita;

use Illuminate\Support\Facades\Log;



class ReceitaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Receita::all();
    }

    public function avaliacoes($id)
    {
        $receita = Receita::find($id);
        return $receita->avaliacoes;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $request->validate([
            'file' => 'required|mimes:jpg,jpeg,png,|max:2048'
         ]);

         $file_name = time().'_'.$request->file->getClientOriginalName();
         $file_path = $request->file('file')->storeAs('uploads', $file_name, 'public');

         $receita =  Receita::create([
            'title'          => $request->title,
            'description'         => $request->description,
            'image_url'         => asset('storage/'. $file_path),
            'average'      => 0,
        ]);

        return response($receita, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $receita = Receita::find($id);
        return $receita;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
