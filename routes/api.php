<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\AuthController;

use App\Http\Controllers\ReceitaController;
use App\Http\Controllers\AvaliacaoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/avaliacao', [AvaliacaoController::class, 'store']);


Route::get('/receitas', [ReceitaController::class, 'index']);
Route::get('/receitas/{id}', [ReceitaController::class, 'show']);
Route::get('/receitas/{id}/avaliacoes/', [ReceitaController::class, 'avaliacoes']);

Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('/receitas', [ReceitaController::class, 'store']);
    Route::put('/receitas/{id}', [ReceitaController::class, 'update']);
    Route::delete('/receitas/{id}', [ReceitaController::class, 'destroy']);
    Route::post('/logout', [AuthController::class, 'logout']);
});
