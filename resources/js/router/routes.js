const Login = () => import("./../views/Login.vue");
const Register = () => import("./../views/Register.vue");
const Dashboard = () => import("./../views/Dashboard.vue");

const Home = () => import("./../views/Home.vue");
const Index = () => import("./../components/Home/Index.vue");
const ReceitaDetails = () => import("./../components/Home/ReceitaDetails.vue");

let LoginRoute = {
    path: "/login",
    component: Login,
    name: "login",
    meta: { auth: false }
};

let RegisterRoute = {
    path: "/register",
    component: Register,
    name: "register",
    meta: { auth: false }
};
let HomeRoute = {
    path: "/home",
    meta: { auth: false },
    name: "home",
    component: Home,
    children: [
        {
            path: "receita/:id/avaliacoes",
            name: "receita_details",
            meta: { auth: false },
            component: ReceitaDetails
        },
        {
            path: "index",
            name: "index",
            component: Index
        }
    ]
};

let DashboardRoutes = {
    path: "/dashboard",
    component: Dashboard,
    meta: {
        auth: true
    },
    name: "Register",
    children: []
};

const routes = [
    LoginRoute,
    RegisterRoute,
    DashboardRoutes,
    HomeRoute,
    { path: "/", redirect: "home/index" },
    { path: "*", redirect: "home/index" }
];

export default routes;
