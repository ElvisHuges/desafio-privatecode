import Vue from "vue";
import VueRouter from "vue-router";
import Router from "vue-router";
//import store from "./../store";

import { getToken } from "./../services/jwt/index";
Vue.use(Router);

import routes from "./routes";

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

router.beforeEach((to, from, next) => {
    const auth = getToken();
    if (to.matched.some(record => record.meta.auth) && !auth) {
        next("/login");
        return;
    }
    if (!to.matched.some(record => record.meta.auth) && auth) {
        next("/dashboard");
        return;
    }
    next();
});

export default router;
