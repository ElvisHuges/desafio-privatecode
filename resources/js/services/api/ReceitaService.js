import { getToken } from "./../jwt/index";
axios.defaults.headers.common.Authorization = "Bearer " + getToken();

export default {
    //Auth
    register(params) {
        return axios.post("/api/register", params);
    },
    login(params) {
        return axios.post("/api/login", params);
    },

    //All Users Requests

    getAllReceitas() {
        return axios.get("/api/receitas");
    },
    getReceita(idReceita) {
        return axios.get(`/api/receitas/${idReceita}`);
    },

    getAllAvaliacaosReceita(idReceita) {
        return axios.get(`/api/receitas/${idReceita}/avaliacoes/`);
    },

    storeReceita(file, title, description) {
        return axios.post("/api/receitas", file, {
            headers: {
                "Content-Type": "multipart/form-data",
                "Access-Control-Allow-Origin": "*",
                Accept: "application/json",
                Authorization: "Bearer " + getToken()
            },
            params: {
                title: title,
                description: description
            }
        });
    }
};
