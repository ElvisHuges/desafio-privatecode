import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

axios.defaults.baseURL = "http://127.0.0.1:8000/";

export default new Vuex.Store({
    state: {
        user: null,
        error: "",
        loadingRequest: false
    },

    getters: {
        isLogged: state => !!state.user,
        loadingRequest: state => state.loadingRequest
    },

    actions: {
        registerUser({ commit }, credentials) {
            console.log("cred", credentials);
            commit("loadingRequest", true);
            return axios.post("user/store", credentials).then(({ data }) => {
                console.log("Resp", data);
                commit("loadingRequest", false);
            });
        },

        login({ commit }, credentials) {
            console.log("cred", credentials);
            return axios.post("login", credentials).then(({ data }) => {
                commit("setUserData", data);
            });
        },

        logout({ commit }) {
            commit("clearUserData");
        }
    },

    mutations: {
        setUserData(state, userData) {
            state.user = userData;
            localStorage.setItem("user", JSON.stringify(userData));
            axios.defaults.headers.common.Authorization = `Bearer ${userData.token}`;
        },

        clearUserData() {
            localStorage.removeItem("user");
            location.reload();
        },
        setError(state, data) {
            state.error = "";
        },
        loadingRequest(state, data) {
            state.loadingRequest = data;
        }
    }
});
